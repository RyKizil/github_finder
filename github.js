/*
 * @Author: Ry.Kzlms 
 * @Date: 2018-02-22 10:12:01 
 * @Last Modified by: Ry.Kzlms
 * @Last Modified time: 2018-02-22 11:46:24
 */
class Github {
    constructor(){
        //Limited to 100 search requests per hour, put your free id here and in url for unlimited searches
        this.client_id = '';
        this.client_text= '';
        this.repos_count = 5;
        this.repos_sort = 'created: asc';

    }
    async getUser(user){
        const profileResponse = await fetch(`https://api.github.com/users/${user}`);
        const repoResponse = await fetch(`https://api.github.com/users/${user}/repos?per_page=${this.repos_count.count}&sort=${this.repos_sort}`);
        
        
        const profile = await profileResponse.json();

        const repos = await repoResponse.json();

        return {
            profile,
            repos
        }
    }


}